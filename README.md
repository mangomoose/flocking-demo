# Flocking Demo

A demo of the [flocking simulation library](https://bitbucket.org/mangomoose/flocking/src/master/) by Mango Moose.

Built with [Pixi.js](https://pixijs.io)