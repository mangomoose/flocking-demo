import * as PIXI from 'pixi.js'

const flockerShadowSprites = [];
const flockerSprites = [];
let overlaySprite;
let displacementMapSprite;

const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
const app = new PIXI.Application({
  width: windowWidth, 
  height: windowHeight,
});
document.body.appendChild(app.view);

function load() {
  app.loader
    .add([
      "img/background.png",
      "img/displacement_map.png",
      "img/rock.png",
      "img/fish_shadow.png",
      "img/fish.png",
      "img/overlay.png",
      "img/tint.png",
    ])
    .load(setup);
}

function setupAudio() {
  let muted = true;
  const audio = new Audio('audio/waves.mp3');
  audio.loop = true;

  const muteIcon = `<svg class="bi bi-volume-mute" width="100%" height="100%" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M6.717 3.55A.5.5 0 017 4v8a.5.5 0 01-.812.39L3.825 10.5H1.5A.5.5 0 011 10V6a.5.5 0 01.5-.5h2.325l2.363-1.89a.5.5 0 01.529-.06zM6 5.04L4.312 6.39A.5.5 0 014 6.5H2v3h2a.5.5 0 01.312.11L6 10.96V5.04zm7.854.606a.5.5 0 010 .708l-4 4a.5.5 0 01-.708-.708l4-4a.5.5 0 01.708 0z" clip-rule="evenodd"/>
    <path fill-rule="evenodd" d="M9.146 5.646a.5.5 0 000 .708l4 4a.5.5 0 00.708-.708l-4-4a.5.5 0 00-.708 0z" clip-rule="evenodd"/>
  </svg>`;
  const unmuteIcon = `<svg class="bi bi-volume-up" width="100%" height="100%" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M6.717 3.55A.5.5 0 017 4v8a.5.5 0 01-.812.39L3.825 10.5H1.5A.5.5 0 011 10V6a.5.5 0 01.5-.5h2.325l2.363-1.89a.5.5 0 01.529-.06zM6 5.04L4.312 6.39A.5.5 0 014 6.5H2v3h2a.5.5 0 01.312.11L6 10.96V5.04z" clip-rule="evenodd"/>
    <path d="M11.536 14.01A8.473 8.473 0 0014.026 8a8.473 8.473 0 00-2.49-6.01l-.708.707A7.476 7.476 0 0113.025 8c0 2.071-.84 3.946-2.197 5.303l.708.707z"/>
    <path d="M10.121 12.596A6.48 6.48 0 0012.025 8a6.48 6.48 0 00-1.904-4.596l-.707.707A5.483 5.483 0 0111.025 8a5.483 5.483 0 01-1.61 3.89l.706.706z"/>
    <path d="M8.707 11.182A4.486 4.486 0 0010.025 8a4.486 4.486 0 00-1.318-3.182L8 5.525A3.489 3.489 0 019.025 8 3.49 3.49 0 018 10.475l.707.707z"/>
  </svg>`;

  const button = document.createElement('button');
  function toggleMute() {
    if (muted) {
      audio.play();
    } else {
      audio.pause();
    }
    muted = !muted;
    button.innerHTML = muted ? muteIcon : unmuteIcon;
  }
  button.innerHTML = muteIcon;
  button.onclick = toggleMute;
  button.id = 'muteButton';
  document.body.appendChild(button);
}

function setup() {
  setupAudio();

  let background = new PIXI.TilingSprite(app.loader.resources["img/background.png"].texture, 128, 128);
  background.width = windowWidth;
  background.height = windowHeight;
  app.stage.addChild(background);

  let rock = new PIXI.Sprite(app.loader.resources["img/rock.png"].texture);
  rock.width = 300;
  rock.height = 300;
  rock.x = windowWidth * 0.75;
  rock.y = windowHeight * 0.25;
  rock.anchor.x = 0.5;
  rock.anchor.y = 0.5;
  app.stage.addChild(rock);

  const flockerShadowContainer = new PIXI.Container();
  app.stage.addChild(flockerShadowContainer);
  const flockerContainer = new PIXI.Container();
  app.stage.addChild(flockerContainer);
  for (let i = 0; i < 200; i++) {
    const flockerConfig = {
      width: 40,
      height: 20,
      position: { x: Math.random() * (windowWidth * 0.9) + 50, y: Math.random() * (windowHeight * 0.875) + 50 },
      velocity: { x: Math.random() * 2 - 1, y: Math.random() * 2 - 1 },
    }
    window.fl.config.FLOCKERS.push(new window.fl.Flocker(flockerConfig));

    let flockerShadowSprite = new PIXI.Sprite(app.loader.resources[`img/fish_shadow.png`].texture);
    flockerShadowSprite.x = flockerConfig.position.x;
    flockerShadowSprite.y = flockerConfig.position.y;
    flockerShadowSprite.anchor.x = 0.5;
    flockerShadowSprite.anchor.y = 0.5;
    flockerShadowSprite.width = flockerConfig.width;
    flockerShadowSprite.height = flockerConfig.height;
    flockerShadowSprite.alpha = 0.5;
    flockerShadowContainer.addChild(flockerShadowSprite);
    flockerShadowSprites.push(flockerShadowSprite);

    let flockerSprite = new PIXI.Sprite(app.loader.resources[`img/fish.png`].texture);
    flockerSprite.x = flockerConfig.position.x;
    flockerSprite.y = flockerConfig.position.y;
    flockerSprite.anchor.x = 0.5;
    flockerSprite.anchor.y = 0.5;
    flockerSprite.width = flockerConfig.width;
    flockerSprite.height = flockerConfig.height;
    flockerContainer.addChild(flockerSprite);
    flockerSprites.push(flockerSprite);
  }
  
  const attractForceFieldConfig = {
    position: {
      x: windowWidth * 0.4,
      y: windowHeight * 0.625
    },
    diameter: 300,
    strength: -0.005
  };
  window.fl.config.FORCE_FIELDS.push(new window.fl.ForceField(attractForceFieldConfig));
  
  const repulseForceFieldConfig = {
    position: {
      x: windowWidth * 0.75,
      y: windowHeight * 0.25
    },
    diameter: 350,
    strength: 2
  };
  window.fl.config.FORCE_FIELDS.push(new window.fl.ForceField(repulseForceFieldConfig));

  const tint = new PIXI.Sprite(app.loader.resources["img/tint.png"].texture);
  tint.width = windowWidth;
  tint.height = windowHeight;
  app.stage.addChild(tint);
  
  overlaySprite = new PIXI.TilingSprite(app.loader.resources[`img/overlay.png`].texture, windowWidth, windowHeight);
  app.stage.addChild(overlaySprite);
  
  displacementMapSprite = new PIXI.TilingSprite(app.loader.resources[`img/displacement_map.png`].texture, windowWidth, windowHeight);
  app.stage.addChild(displacementMapSprite);
  const displacementMapFilter = new PIXI.filters.DisplacementFilter(displacementMapSprite, 20);
  app.stage.filters = [displacementMapFilter];

  window.fl.config.NEIGHBOUR_RADIUS = 40;
  window.fl.config.SEPARATION_RADIUS = 40;

  window.fl.config.ALIGNMENT_WEIGHT = 0.25;
  window.fl.config.COHESION_WEIGHT = 0.45;
  window.fl.config.SEPARATION_WEIGHT = 1.5;
  window.fl.config.FORCE_FIELD_WEIGHT = 1;
  
  window.fl.config.MAX_VELOCITY = 2,
  window.fl.config.WORLD_WIDTH = windowWidth;
  window.fl.config.WORLD_HEIGHT = windowHeight;

  app.ticker.add(loop);

  window.fl.setConfig(function() {
    window.fl.ready(function() {
      window.fl.start();
    });
  });
}

function loop() {
  displacementMapSprite.tilePosition.x -= 0.5;
  displacementMapSprite.tilePosition.y -= 0.5;
  overlaySprite.tilePosition.x -= 0.5;
  overlaySprite.tilePosition.y -= 0.5;

  for (let i = 0; i < window.fl.config.FLOCKERS.length; i++) {
    const flocker = window.fl.config.FLOCKERS[i];

    const flockerShadowSprite = flockerShadowSprites[i];
    flockerShadowSprite.x = flocker.position.x - 5;
    flockerShadowSprite.y = flocker.position.y - 5;
    flockerShadowSprite.rotation = flocker.rotation + (Math.PI / 2);

    const flockerSprite = flockerSprites[i];
    flockerSprite.x = flocker.position.x;
    flockerSprite.y = flocker.position.y;
    flockerSprite.rotation = flocker.rotation + (Math.PI / 2);
  }
};

load();
