const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  output: {
    filename: 'flocking-demo.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new CopyPlugin([
      { from: 'img/*', to: '' },
      { from: 'audio/*', to: '' },
      { from: 'css/*', to: '' },
      { from: 'html', to: '' },
    ]),
  ],
};